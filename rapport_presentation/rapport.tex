\documentclass[a4paper, 12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage[T1]{fontenc}
\usepackage{setspace}
\usepackage{graphicx}
\usepackage{color}
\usepackage{authblk}
\usepackage{float}
\usepackage{shapepar}
\usepackage{enumitem}
\setcounter{secnumdepth}{4}
\frenchbsetup{StandardLists=true}
\usepackage{blindtext}
\usepackage{scrextend}
\addtokomafont{labelinglabel}{\sffamily}
\usepackage{calc}
\usepackage{minted}
\usemintedstyle{colorful}
\floatplacement{figure}{H}
\usepackage[left=2.5cm,top=2.5cm,right=2.5cm,bottom=2.5cm]{geometry}
\usepackage{caption}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=blue
}
\usepackage{verbatim}

\begin{document}
\title{Compilateur Hepial - JVM}
\author{Raed Abdennadher et Steven Liatti}
\affil{\small Techniques de compilation - Prof. Stéphane Malandain}
\affil{\small Hepia ITI 2\up{ème} année}
\date{Printemps 2017}
\maketitle

\begin{figure}
	\begin{center}
		\includegraphics[width=0.8\textwidth]{images/compilateur.jpg}
	\end{center}
	% \caption*{Compilation d'un code source - \url{http://bit.ly/2tuZhKh}}
\end{figure}

\begin{figure}[!b]
	\centering
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[width=.7\linewidth]{images/hepia.jpg}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[width=.7\linewidth]{images/hesso.jpg}
	\end{minipage}
\end{figure}
\newpage

\setcounter{tocdepth}{4}
\tableofcontents
\listoffigures
\renewcommand\listoflistingscaption{Table des listings de code source}
\listoflistings
\newpage

\section{Introduction}
Le but de ce travail était de développer un compilateur recevant du code source en langage hepial pour le traduire
en bytecode Java interprété par la machine virtuelle Java. L'hepial est un langage procédural créé pour l'occasion
par notre professeur. Il est basé sur le BASIC ou Ada, ses mot-clés d'instructions sont en français. Voici un
exemple de code :
\begin{listing}[ht]
	\inputminted[breaklines,breaksymbol=,linenos,frame=leftline,stepnumber=1,tabsize=2]{text}{../src/exemple_code_hepial/hepial3}
	\caption{Exemple de code source hepial}
\end{listing}
\newline
L'objectif était de faire une analyse lexicale et syntaxique avec JFlex (\url{http://jflex.de/}) et Cup
(\url{http://www2.cs.tum.edu/projects/cup/}) respectivement. JFlex produit des lexèmes qui vont être "consommés"
par Cup. Finalement, nous avons généré du bytecode Java grâce à Jasmin (\url{http://jasmin.sourceforge.net/}), un
langage assembleur pour la machine virtuelle Java. En effet, plutôt que de générer les instructions Java finales
(contenu d'un fichier .class) nous avons fait cette étape de simplificaiton intermédiaire, le but n'étant pas
de comprendre comment fonctionnait la JVM dans les moindres détails.

\section{Grammaire}
Pour rappel, une grammaire est constituée d'un ensemble de symboles terminaux, d'un autre de non-terminaux, d'un
symbole non terminal de départ et d'un ensemble de règles de production. Chomsky classe les grammaires selon 4 types
différents :
\begin{itemize}
	\item Type 0 : pas de restrictions sur les productions
	\item Type 1 : grammaires sensibles au contexte
	\item Type 2 : grammaires hors contexte
	\item Type 3 : grammaires régulières
\end{itemize}
Pour cette première étape de notre projet, nous avons du coder la grammaire du langage. Cette étape n'était pas
difficile, la première version de la grammaire nous était fournie, nous avons du faire quelques adaptations liées
à Cup (pour la règle \no{2} (PROGRAMME), \no{4} (DECLA) et \no{8} (DECLAFONC)). Enfin, il faut préciser que Cup fait
une analyse ascendante du code source, c'est-à-dire qu'il part des lexèmes générés par JFlex pour tenter de retrouver
l'axiome, l'élément initial.

\section{Construction de la table des symboles}
La table des symboles (TDS) est une table associative avec les entrées comme clés et une pile d'association comme valeurs
(\mintinline{Java}{HashMap<Entree, Stack<Association>>}) :
\begin{itemize}
    \item Une entrée est représentée par la classe \mintinline{Java}{Entree} et ses sous-classes, qui définissent
    les identificateurs des variables déclarées.
    \item Une association est représentée par la classe \mintinline{Java}{Association}, qui associe un symbole et
    son numéro de bloc. Le numéro de bloc représente la localisation de la déclaration du symbole. Par exemple, si
    on déclare une variable dans le programme principal, le numéro de bloc vaudra 0. Et si on la déclare
    dans une fonction, le bloc sera égal à 1, etc. Les doubles déclarations sont détectées et
    les déclarations locales à un sous-bloc sont gérées (plusieurs variables de même nom peuvent ainsi être utilisées,
    elles seront perçues différemment selon le bloc considéré).
    \item Un symbole est représenté par la classe \mintinline{Java}{Symbole} et ses sous-classes, qui définissent
    le type et la ligne d'une déclaration.
    \item Un type est représenté par la classe \mintinline{Java}{Type} et ses sous-classes, qui définissent le type
    d'une entrée (entier, booléen, tableau, fonction ou programme entier dans notre cas). Les traitements diffèrent
    légèrement d'un type de déclaration à un autre. Prenons l'exemple de \mintinline{Java}{TypeFonction}, gérant les
	arguments d'une fonction : une information supplémentaire est sauvegardée, l'indice d'un argument dans la liste des
	arguments.
\end{itemize}
Cette table est construite selon une analyse syntaxique descendante (contrairement à celle de CUP).
Grâce à une pile, elle parcoure ainsi le code source en ajoutant les nouvelles entrées ou en modifiant celles déjà
rencontrées. L'appel à la méthode \mintinline{Java}{ajouter(Entree e, Symbole s)} de \mintinline{text}{TDS.java} peut
mener vers deux scénarios :
\begin{enumerate}
    \item Si l'entrée de la déclaration (l'identificateur) n'existe pas dans la TDS, cela veut dire que cet identicateur
    n'est pas utilisé auparavant. Dans ce cas, elle sera insérée, avec comme valeur une pile qui contient un seul
    élément \mintinline{java}{Association}.
    \item Si l'entrée existe, cela veut dire qu'elle est déjà rencontrée dans le code source. Alors, on récupère
    le sommet de la pile pour vérifier son numéro de bloc. Si ce dernier est égal au numéro du bloc courant, la variable
	est déjà déclarée dans ce bloc, et donc une double déclaration est détectée. Enfin, si son numéro
    de bloc est différent du numéro du bloc courant, alors cette variable est déclarée auparavant, mais dans un autre bloc.
    Donc on insère une nouvelle association ayant le numéro du bloc courant.
\end{enumerate}
Dans la figure \ref{tds}, on peut voir toutes les relations entre la table des symboles, les entrées, les symboles et
les types sous forme de diagramme UML.
\begin{figure}
	\begin{center}
		\includegraphics[width=1.0\textwidth]{images/tds4.jpg}
	\end{center}
	\caption{Diagramme de la table des symboles et ses sous-classes}
	\label{tds}
\end{figure}

\newpage
\section{Construction de l'arbre abstrait}
Une fois la table des symboles faite, il a fallut construire l'arbre abstrait. En parcourant le code source de manière
ascendante avec Cup (des feuilles à la racine de l'arbre), chaque \mintinline{Java}{Expression} et chaque
\mintinline{Java}{Instruction} sont stockées dans l'arbre (en réalité une pile d'\mintinline{Java}{ArbreAbstrait}).
Pour assembler une expression ou une instruction de niveau supérieur, il faut dépiler le bon nombre de sous-arbres,
exécuter l'opération (logique, affectation, arithmétique, etc.) puis réempiler l'expression ou instruction de niveau
supérieur ainsi créée. Explications par l'exemple : prenons le cas du "si ... alors ... sinon" en figure \ref{si_alors}.
\begin{figure}
	\begin{center}
		\includegraphics[width=1.0\textwidth]{images/si_alors.png}
	\end{center}
	\caption{Illustration d'un empilement/dépilement d'un "si ... alors ... sinon"}
	\label{si_alors}
\end{figure}
Un "si ... alors ... sinon" se compose de :
\begin{itemize}
    \item Une \mintinline{Java}{Expression} qui représente la condition de la structure "si".
    \item Un premier \mintinline{Java}{Bloc} (pour le "alors") qui représente la liste des instructions à exécuter
    si la condition est vraie.
    \item Un second \mintinline{Java}{Bloc} (pour le "sinon") qui représente la liste des instructions à exécuter
    si la condition est fausse.
\end{itemize}
Comme l'analyse de \mintinline{text}{CUP} est ascendante, après avoir dérivé la règle pour le non-terminal
\mintinline{Java}{CONDITION}, le bloc "sinon" se trouve au sommet de la \mintinline{Java}{pileArbre}, suivi du
bloc "alors" et enfin de l'expression de la condition. Chacun de ses trois éléments est composé de
sous-instructions ou expressions déjà construites. Une fois dépilées dans cet ordre, un objet \mintinline{Java}{Condition}
est créée avec ces 3 éléments et est à nouveau empilé dans l'arbre. Le listing \ref{code:si_alor_cup} met en oeuvre
ce raisonnement.
\begin{listing}[ht]
    \inputminted[breaklines,breaksymbol=,linenos,frame=leftline,stepnumber=1,tabsize=2,firstline=344,lastline=353]{Java}{../src/grammaire.cup}
    \caption{Extrait du code CUP : la règle pour la structure conditionnelle}
    \label{code:si_alor_cup}
\end{listing}
Chaque sous-arbre est traité de cette manière, pour n'avoir qu'un seul élément (\mintinline{Java}{ProgPrincipal})
dans la pile à la toute fin de l'analyse. Comme pour la TDS, un diagramme (partiel) de l'arbre abstrait est représenté
en figure \ref{arbre}.
\begin{figure}
	\begin{center}
		\includegraphics[width=0.9\textwidth]{images/arbre2.jpg}
	\end{center}
	\caption{Diagramme partiel de l'arbre abstrait et ses sous-classes}
	\label{arbre}
\end{figure}


\section{Analyse sémantique}
    Après avoir construit la table des symboles ainsi que l'arbre abstrait du code source, nous passons à la
    phase de l'analyse sémantique. C'est une technique proche de l'analyse lexicale, mais au lieu de
    se faire au niveau des mots, le contrôle se fait sur la sémantique des phrases pour déterminer le sens des
    écrits. Dans le cadre d'un langage de programmation, les phrases représentent les différentes instructions
    qui construisent le programme. L'analyse sémantique sert donc à vérifier si les types sont conformes. Par exemple,
    pour réaliser une affectation, il faut que le type de la source soit conforme au type de la destination.
    En cas de contradiction, le compilateur doit pouvoir la détecter et afficher un message d'erreur.

    \subsection{Méthodologie}
    Pour effectuer le contrôle sémantique, nous avons utilisé le patron de conception \mintinline{text}{Visiteur} : c'est
    un patron de comportement qui permet d'externaliser et de centraliser des actions à effectuer sur des objets
    qui n'ont pas forcément de liens entre eux. Ces actions ne seront pas implémentées directement dans la classe
    de ces objets, mais dans des classes externes. On va expliquer le fonctionnement de ce patron de conception
    en l'appliquant directement pour notre analyseur sémantique.
    \subsection{Réalisation}
    Tout d'abord, nous avons créé l'interface \mintinline{Java}{IVisiteur} (listing \ref{code:IVisiteur}). Cette interface
    va structurer les classes qui vont l'implémenter.

    \begin{listing}[ht]
    	\inputminted[breaklines,breaksymbol=,linenos,frame=leftline,stepnumber=1,tabsize=2,firstline=5,lastline=7]{Java}{../src/semantique/IVisiteur.java}
        \vdots
        \inputminted[breaklines,breaksymbol=,linenos,frame=leftline,stepnumber=1,tabsize=2,firstline=11,lastline=12]{Java}{../src/semantique/IVisiteur.java}
        \vdots
    	\caption{Extrait de la classe \mintinline{Java}{IVisiteur}}
        \label{code:IVisiteur}
    \end{listing}

	La classe \mintinline{Java}{AnalyseurSemantique.java} contient les traitements concrets qui vont être effectués
	pour vérifier :
    \begin{itemize}[label=-]
        \item les identificateurs utilisés : dans le cas de l'utilisation d'une variable ou l'appel à
        une fonction non déclarée.
        \item les types de la source et de la destination d'une affectation.
        \item les types des opérandes d'une expression binaire.
        \item le type de retour d'une expression dans la condition de la structure "while" par exemple qui doit être
        un \mintinline{Java}{TypeBooleen}.
        \item le type de l'index d'un tableau qui doit être un \mintinline{Java}{TypeEntier}.
        \item le type de la variable indéxée qui doit être un \mintinline{Java}{TypeTableau}.
        \item le type de l'identificateur utilisé pour un appel de fonction qui doit être un \mintinline{Java}{TypeFonction}.
        \item le type de retour d'une fonction.
        \item le type et ordre des paramètres passés à une fonction.
    \end{itemize}
    La vérification de la structure conditionnelle "si" (listing \ref{code:condition_java}) se passe de la manière
    suivante : on vérifie si la condition est bien de type booléen. Ensuite, on vérifie le bloc "alors".
    Enfin, on vérifie le bloc "sinon" si présent.

    \begin{listing}[ht]
    	\inputminted[breaklines,breaksymbol=,linenos,frame=leftline,stepnumber=1,tabsize=2,firstline=154,lastline=164]{Java}{../src/semantique/AnalyseurSemantique.java}
    	\caption{Extrait de la classe \mintinline{Java}{AnalyseurSemantique} : analyse du "si"}
        \label{code:condition_java}
    \end{listing}

    Pour pouvoir enregistrer toutes les erreurs sémantiques détectées, nous avons créé la classe
    \mintinline{Java}{FlotErreurs}. Elle respecte le patron de conception \mintinline{Java}{Singleton} en ayant
	une seule instance déclarée. Cette classe hérite de \mintinline{Java}{Stack<String>} et admet
    une redéfinition de la méthode \mintinline{Java}{toString()}. Au moment de la détection d'une erreur, une nouvelle
    chaîne de caractères sera insérée via une méthode (spécifique à l'erreur) de la classe
    \mintinline{Java}{ErreurSemantique} qui ne contient que des méthodes \mintinline{Java}{public static}.
    Chacune de ses méthodes retourne une chaîne de caractères expliquant la cause de l'erreur en question.
    Le listing \ref{code:ErreurSem_java} montre l'exemple d'une erreur de conformité de types entre deux opérandes
    d'une expression binaire.

    \begin{listing}[ht]
    	\inputminted[breaklines,breaksymbol=,linenos,frame=leftline,stepnumber=1,tabsize=2,firstline=18,lastline=20]{Java}{../src/semantique/ErreurSemantique.java}
    	\caption{Extrait de la classe \mintinline{Java}{ErreurSemantique} : types expression binaire non conformes}
        \label{code:ErreurSem_java}
    \end{listing}

\section{Génération du code assembleur Jasmin}
    L'étape finale du projet consistait à générer du code assembleur \mintinline{text}{Jasmin}.
    Pour y parvenir, nous avons utilisé la classe \mintinline{Java}{GenerateurCodeJasmin} qui implémente l'interface
    \mintinline{Java}{IVisiteur} et qui respecte le patron de conception \mintinline{Java}{Visiteur}.
    Le visiteur de génération du code va naviguer dans notre arbre abrtrait, et ajouter des instructions
    \mintinline{text}{Jasmin} au fur et à mesure. L'ensemble est enregistré dans l'instance unique de la classe
    \mintinline{Java}{TexteCible} qui hérite de \mintinline{Java}{ArrayList<String>} et qui respecte le patron
    de conception \mintinline{Java}{Singleton}.
    \newline
    Pour illustrer nos propos, prenons l'exemple de la production du code assembleur de la structure conditionnelle "si"
    via la méthode \mintinline{Java}{visiter(Condition c)} (listing \ref{code:condition_jasmin}). Pendant l'exécution,
    si l'appel \mintinline{Java}{c.condition().accepter(this);} avait empilé 1 dans la pile des opérandes
    de la JVM, alors il y aura un saut au bloc "alors". Sinon, il y aura un saut au bloc "sinon".

    \begin{listing}[ht]
        \inputminted[breaklines,breaksymbol=,linenos,frame=leftline,stepnumber=1,tabsize=2,firstline=219,lastline=232]{Java}{../src/semantique/GenerateurCodeJasmin.java}
        \caption{Extrait de la classe \mintinline{Java}{GenerateurCodeJasmin} : la structure conditionnelle}
        \label{code:condition_jasmin}
    \end{listing}

\section{Conclusion}
En conclusion, à partir de la grammaire du langage hepial fournie, nous avons pu mettre en place une table
des symboles qui représente une association entre une entrée et un symbole dans le but de construire la
liste des déclarations de variables et de fonctions. Grâce à cette TDS, nous avons produit l'arbre abstrait
représenté sous forme de pile qui nous a permis de transformer le code source en arbres d'instructions,
formant le programme. Ensuite, nous avons vérifié la sémantique des instructions de ce dernier avec le
patron de conception Visiteur. Enfin, nous avons pu traduire le code source donné en assembleur Jasmin.
\newline
C'était une expérience intéressante intellectuellement, nous avons découvert toute une partie de la
programmation que nous ne connaissions pas auparavant.


\end{document}
