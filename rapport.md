# Compilateur en JVM du langage `Hepial` (utilisant JFlex et Cup et Jasmin)

> Raed Abdenandher & Steven Liatti
>
> Techniques de compilation - Prof. Stéphane Malandain
>
> Hepia ITI 2ème année | 2017

![image](./rapport_presentation/images/compilateur.jpg)

| ![](./rapport_presentation/images/hepia.jpg) | ![](./rapport_presentation/images/hesso.jpg) |
| ------------------------------------------------------------ | ------------------------------------------------------------ |

Introduction
============

Le but de ce travail était de développer un compilateur recevant du code source en langage hepial pour le traduire en bytecode Java interprété par la machine virtuelle Java. L’hepial est un langage procédural créé pour l’occasion par notre professeur. Il est basé sur le BASIC ou Ada, ses mot-clés d’instructions sont en français. Voici un exemple de code :

```
programme hepial3

constante entier FIXE = 50;
entier nb;

entier carre(entier i)
    entier test;
    debutfonc
        i = i * i;
        retourne i;
    finfonc

debutprg
    si nb >= FIXE alors
        nb = 5;
    sinon
        nb = 3;
    finsi
    ecrire carre(nb); 
finprg
```

*Listing 1 – Exemple de code source hepial*

L’objectif était de faire une analyse lexicale et syntaxique avec JFlex (<http://jflex.de/>) et Cup (<http://www2.cs.tum.edu/projects/cup/>) respectivement. JFlex produit des lexèmes qui vont être “consommés” par Cup. Finalement, nous avons généré du bytecode Java grâce à Jasmin (<http://jasmin.sourceforge.net/>), un langage assembleur pour la machine virtuelle Java. En effet, plutôt que de générer les instructions Java finales (contenu d’un fichier .class) nous avons fait cette étape de simplificaiton intermédiaire, le but n’étant pas de comprendre comment fonctionnait la JVM dans les moindres détails. 

Grammaire
=========

Pour rappel, une grammaire est constituée d’un ensemble de symboles terminaux, d’un autre de non-terminaux, d’un symbole non terminal de départ et d’un ensemble de règles de production. Chomsky classe les grammaires selon 4 types différents :

-   Type 0 : pas de restrictions sur les productions

-   Type 1 : grammaires sensibles au contexte

-   Type 2 : grammaires hors contexte

-   Type 3 : grammaires régulières

Pour cette première étape de notre projet, nous avons du coder la grammaire du langage. Cette étape n’était pas difficile, la première version de la grammaire nous était fournie, nous avons du faire quelques adaptations liées à Cup (pour la règle $`n°2`$ (PROGRAMME), $`n°4`$ (DECLA) et $`n°8`$ (DECLAFONC)). Enfin, il faut préciser que Cup fait une analyse ascendante du code source, c’est-à-dire qu’il part des lexèmes générés par JFlex pour tenter de retrouver l’axiome, l’élément initial. 
Construction de la table des symboles
=====================================

La table des symboles (TDS) est une table associative avec les entrées comme clés et une pile d’association comme valeurs (`HashMap<Entree, Stack<Association>>`) :

-   Une entrée est représentée par la classe `Entree` et ses sous-classes, qui définissent les identificateurs des variables déclarées.

-   Une association est représentée par la classe `Association` , qui associe un symbole et son numéro de bloc. Le numéro de bloc représente la localisation de la déclaration du symbole. Par exemple, si on déclare une variable dans le programme principal, le numéro de bloc vaudra 0. Et si on la déclare dans une fonction, le bloc sera égal à 1, etc. Les doubles déclarations sont détectées et les déclarations locales à un sous-bloc sont gérées (plusieurs variables de même nom peuvent ainsi être utilisées, elles seront perçues différemment selon le bloc considéré).

-   Un symbole est représenté par la classe `Symbole` et ses sous-classes, qui définissent le type et la ligne d’une déclaration.

-   Un type est représenté par la classe `Type` et ses sous-classes, qui définissent le type d’une entrée (entier, booléen, tableau, fonction ou programme entier dans notre cas). Les traitements diffèrent légèrement d’un type de déclaration à un autre. Prenons l’exemple de `TypeFonction` , gérant les arguments d’une fonction : une information supplémentaire est sauvegardée, l’indice d’un argument dans la liste des arguments.

Cette table est construite selon une analyse syntaxique descendante (contrairement à celle de CUP). Grâce à une pile, elle parcoure ainsi le code source en ajoutant les nouvelles entrées ou en modifiant celles déjà rencontrées. L’appel à la méthode `ajouter(Entree e, Symbole s)` de `TDS.java` peut mener vers deux scénarios :

1.  Si l’entrée de la déclaration (l’identificateur) n’existe pas dans la TDS, cela veut dire que cet identicateur n’est pas utilisé auparavant. Dans ce cas, elle sera insérée, avec comme valeur une pile qui contient un seul élément `Association`.

2.  Si l’entrée existe, cela veut dire qu’elle est déjà rencontrée dans le code source. Alors, on récupère le sommet de la pile pour vérifier son numéro de bloc. Si ce dernier est égal au numéro du bloc courant, la variable est déjà déclarée dans ce bloc, et donc une double déclaration est détectée. Enfin, si son numéro de bloc est différent du numéro du bloc courant, alors cette variable est déclarée auparavant, mais dans un autre bloc. Donc on insère une nouvelle association ayant le numéro du bloc courant.

Dans la figure 1, on peut voir toutes les relations entre la table des symboles, les entrées, les symboles et les types sous forme de diagramme UML.

![image](./rapport_presentation/images/tds4.jpg)

*Figure 1 – Diagramme de la table des symboles et ses sous-classes*

Construction de l’arbre abstrait
================================

Une fois la table des symboles faite, il a fallut construire l’arbre abstrait. En parcourant le code source de manière ascendante avec Cup (des feuilles à la racine de l’arbre), chaque `Expression` et chaque `Instruction` sont stockées dans l’arbre (en réalité une pile d’`ArbreAbstrait`). Pour assembler une expression ou une instruction de niveau supérieur, il faut dépiler le bon nombre de sous-arbres, exécuter l’opération (logique, affectation, arithmétique, etc.) puis réempiler l’expression ou instruction de niveau supérieur ainsi créée. Explications par l’exemple : prenons le cas du “si ... alors ... sinon” en figure 2.

![image](./rapport_presentation/images/si_alors.png)

*Figure 2 – Illustration d’un empilement/dépilement d’un "si ... alors ... sinon"*

Un “si ... alors ... sinon” se compose de :

-   Une `Expression` qui représente la condition de la structure “si”.

-   Un premier `Bloc` (pour le “alors”) qui représente la liste des instructions à exécuter si la condition est vraie.

-   Un second `Bloc` (pour le “sinon”) qui représente la liste des instructions à exécuter si la condition est fausse.

Comme l’analyse de `CUP` est ascendante, après avoir dérivé la règle pour le non-terminal `CONDITION`, le bloc “sinon” se trouve au sommet de la `pileArbre`, suivi du bloc “alors” et enfin de l’expression de la condition. Chacun de ses trois éléments est composé de sous-instructions ou expressions déjà construites. Une fois dépilées dans cet ordre, un objet `Condition` est créée avec ces 3 éléments et est à nouveau empilé dans l’arbre. Le listing 2 met en oeuvre ce raisonnement. Chaque sous-arbre est traité de cette manière, pour n’avoir qu’un seul élément (`ProgPrincipal`) dans la pile à la toute fin de l’analyse. Comme pour la TDS, un diagramme (partiel) de l’arbre abstrait est représenté en figure 3.

```cup
CONDITION ::= si EXPR alors CORPS sinon CORPS finsi {:
 	Bloc sinon = (Bloc) pileArbre.pop();
 	Bloc alors = (Bloc) pileArbre.pop();
 	Expression cond = (Expression) pileArbre.pop();
 	pileArbre.push(new Condition(cond, alors, sinon));
:} | si EXPR alors CORPS finsi {:
 	Bloc alors = (Bloc) pileArbre.pop();
 	Expression cond = (Expression) pileArbre.pop();
 	pileArbre.push(new Condition(cond, alors, null));
:};
```

*Listing 2 – Extrait du code CUP : la règle pour la structure conditionnelle*

![](./rapport_presentation/images/arbre2.jpg)

*Figure 3 – Diagramme partiel de l’arbre abstrait et ses sous-classes*

Analyse sémantique
==================

Après avoir construit la table des symboles ainsi que l’arbre abstrait du code source, nous passons à la phase de l’analyse sémantique. C’est une technique proche de l’analyse lexicale, mais au lieu de se faire au niveau des mots, le contrôle se fait sur la sémantique des phrases pour déterminer le sens des écrits. Dans le cadre d’un langage de programmation, les phrases représentent les différentes instructions qui construisent le programme. L’analyse sémantique sert donc à vérifier si les types sont conformes. Par exemple, pour réaliser une affectation, il faut que le type de la source soit conforme au type de la destination. En cas de contradiction, le compilateur doit pouvoir la détecter et afficher un message d’erreur.

Méthodologie
------------

Pour effectuer le contrôle sémantique, nous avons utilisé le patron de conception `Visiteur` : c’est un patron de comportement qui permet d’externaliser et de centraliser des actions à effectuer sur des objets qui n’ont pas forcément de liens entre eux. Ces actions ne seront pas implémentées directement dans la classe de ces objets, mais dans des classes externes. On va expliquer le fonctionnement de ce patron de conception en l’appliquant directement pour notre analyseur sémantique.

Réalisation
-----------

Tout d’abord, nous avons créé l’interface `IVisiteur` (listing 3). Cette interface va structurer les classes qui vont l’implémenter.

```java
public interface IVisiteur {
     public Object visiter(ProgPrincipal p);
	public Object visiter(Fonction f);
	.
	.
	public Object visiter(Affectation a);
	public Object visiter(Nombre n);
	.
	.
```

*Listing 3 – Extrait de la classe IVisiteur*

La classe `AnalyseurSemantique` contient les traitements concrets qui vont être effectués pour vérifier :

-   les identificateurs utilisés : dans le cas de l’utilisation d’une variable ou l’appel à une fonction non déclarée.

-   les types de la source et de la destination d’une affectation.

-   les types des opérandes d’une expression binaire.

-   le type de retour d’une expression dans la condition de la structure “while” par exemple qui doit être un `TypeBooleen`.

-   le type de l’index d’un tableau qui doit être un `TypeEntier`.

-   le type de la variable indéxée qui doit être un `TypeTableau`.

-   le type de l’identificateur utilisé pour un appel de fonction qui doit être un `TypeFonction`.

-   le type de retour d’une fonction.

-   le type et ordre des paramètres passés à une fonction.

La vérification de la structure conditionnelle “si” (listing 4) se passe de la manière suivante : on vérifie si la condition est bien de type booléen. Ensuite, on vérifie le bloc “alors”. Enfin, on vérifie le bloc “sinon” si présent.

````java
public Object visiter(Condition c) {
	c.condition().accepter(this);
	if (c.condition().type() != null) {
		if (c.condition().type() != TypeBooleen.getInstance())
			FlotErreurs.getInstance().push(ErreurSemantique.booleenAttendu(c.condition().ligne()));
	}
	c.alors().accepter(this);
	if (c.sinon() != null)
		c.sinon().accepter(this);
	return null;
}
````

*Listing 4 – Extrait de la classe `AnalyseurSemantique `: analyse du "si"*

Pour pouvoir enregistrer toutes les erreurs sémantiques détectées, nous avons créé la classe `FlotErreurs`. Elle respecte le patron de conception `Singleton` en ayant une seule instance déclarée. Cette classe hérite de `Stack<String>` et admet une redéfinition de la méthode `toString()`. Au moment de la détection d’une erreur, une nouvelle chaîne de caractères sera insérée via une méthode (spécifique à l’erreur) de la classe `ErreurSemantique` qui ne contient que des méthodes `public static`. Chacune de ses méthodes retourne une chaîne de caractères expliquant la cause de l’erreur en question. Le listing 5 montre l’exemple d’une erreur de conformité de types entre deux opérandes d’une expression binaire.

````java
public static String typeNonConformesBinaire(Type typeGauche, Type typeDroite, int ligne) {
	return "ligne " + ligne + " - Erreur sémantique : Expression binaire non permise entre les types " + typeGauche + " et " + typeDroite;
````

*Listing 5 – Extrait de la classe `ErreurSemantique`: types expression binaire non conformes*

Génération du code assembleur Jasmin
====================================

L’étape finale du projet consistait à générer du code assembleur `Jasmin`. Pour y parvenir, nous avons utilisé la classe `GenerateurCodeJasmin` qui implémente l’interface `IVisiteur` et qui respecte le patron de conception `Visiteur`. Le visiteur de génération du code va naviguer dans notre arbre abrtrait, et ajouter des instructions `Jasmin` au fur et à mesure. L’ensemble est enregistré dans l’instance unique de la classe `TexteCible` qui hérite de `ArrayList<String>` et qui respecte le patron de conception`Singleton` . Pour illustrer nos propos, prenons l’exemple de la production du code assembleur de la structure conditionnelle “si” via la méthode `visiter(Condition c)` (listing 6). Pendant l’exécution, si l’appel `c.condition().accepter(this);` avait empilé 1 dans la pile des opérandes de la JVM, alors il y aura un saut au bloc “alors”. Sinon, il y aura un saut au bloc “sinon”.

````java
public Object visiter(Condition c) {
	c.condition().accepter(this);
	cible.add("ifne label" + c.hashCode() + "_0"); // succeeds if and only if value 0
	cible.add("goto label" + c.hashCode() + "_1");
	cible.add("label" + c.hashCode() + "_0:");
	c.alors().accepter(this);
	cible.add("goto endif" + c.hashCode() + "_0");
	cible.add("label" + c.hashCode() + "_1:");
	if (c.sinon() != null) {
		c.sinon().accepter(this);
	}
	cible.add("endif" + c.hashCode() + "_0:");
	return null;
}
````

*Listing 6 – Extrait de la classe `GenerateurCodeJasmin `: la structure conditionnelle*

Conclusion
==========

En conclusion, à partir de la grammaire du langage hepial fournie, nous avons pu mettre en place une table des symboles qui représente une association entre une entrée et un symbole dans le but de construire la liste des déclarations de variables et de fonctions. Grâce à cette TDS, nous avons produit l’arbre abstrait représenté sous forme de pile qui nous a permis de transformer le code source en arbres d’instructions, formant le programme. Ensuite, nous avons vérifié la sémantique des instructions de ce dernier avec le patron de conception Visiteur. Enfin, nous avons pu traduire le code source donné en assembleur Jasmin. 

C’était une expérience intéressante intellectuellement, nous avons découvert toute une partie de la programmation que nous ne connaissions pas auparavant.
