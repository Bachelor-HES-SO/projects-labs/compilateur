
import java_cup.runtime.*;

%%
%class Lexer
%line
%column
%cup

%{
    //envoyer le numéro de la ligne à CUP via le Scanner
    public int getLine() { return yyline + 1; }
%}

ident=[a-zA-Z][a-zA-Z0-9]*
constentier=[-]?[0-9]+
constchaine=[\"].*[\"]
commentaire=\/\/.*\n

%%
/* rules */

"programme" {
    return new Symbol(sym.programme, new String(yytext()));
}

"debutprg" {
    return new Symbol(sym.debutprg, new String(yytext()));
}

"finprg" {
    return new Symbol(sym.finprg, new String(yytext()));
}

"debutfonc" {
    return new Symbol(sym.debutfonc, new String(yytext()));
}

"finfonc" {
    return new Symbol(sym.finfonc, new String(yytext()));
}

"constante" {
    return new Symbol(sym.constante, new String(yytext()));
}

";" {
    return new Symbol(sym.pointvirgule, new String(yytext()));
}

"(" {
    return new Symbol(sym.parouvrante, new String(yytext()));
}

")" {
    return new Symbol(sym.parfermante, new String(yytext()));
}

"=" {
	return new Symbol(sym.egal, new String(yytext()));
}

"," {
    return new Symbol(sym.virgule, new String(yytext()));
}

".." {
    return new Symbol(sym.doublepoint, new String(yytext()));
}

"[" {
    return new Symbol(sym.crochouvrant, new String(yytext()));
}

"]" {
    return new Symbol(sym.crochfermant, new String(yytext()));
}

"+" {
    return new Symbol(sym.plus, new String(yytext()));
}

"-" {
    return new Symbol(sym.moins, new String(yytext()));
}

"~" {
    return new Symbol(sym.tilde, new String(yytext()));
}

"*" {
    return new Symbol(sym.mult, new String(yytext()));
}

"non" {
    return new Symbol(sym.neg, new String(yytext()));
}

"et" {
    return new Symbol(sym.et, new String(yytext()));
}

"ou" {
    return new Symbol(sym.ou, new String(yytext()));
}

"/" {
    return new Symbol(sym.div, new String(yytext()));
}

"%" {
    return new Symbol(sym.mod, new String(yytext()));
}

"==" {
	return new Symbol(sym.doublegal, new String(yytext()));
}

"<>" {
	return new Symbol(sym.different, new String(yytext()));
}

"<" {
	return new Symbol(sym.inf, new String(yytext()));
}

">" {
	return new Symbol(sym.sup, new String(yytext()));
}

"<=" {
	return new Symbol(sym.infegal, new String(yytext()));
}

">=" {
	return new Symbol(sym.supegal, new String(yytext()));
}

"vrai" {
	return new Symbol(sym.vrai, new String(yytext()));
}

"faux" {
	return new Symbol(sym.faux, new String(yytext()));
}

"booleen" {
    return new Symbol(sym.booleen, new String(yytext()));
}

"entier" {
    return new Symbol(sym.entier, new String(yytext()));
}

"si" {
	return new Symbol(sym.si, new String(yytext()));
}

"alors" {
	return new Symbol(sym.alors, new String(yytext()));
}

"sinon" {
	return new Symbol(sym.sinon, new String(yytext()));
}

"finsi" {
	return new Symbol(sym.finsi, new String(yytext()));
}

"tantque" {
	return new Symbol(sym.tantque, new String(yytext()));
}

"fintantque" {
	return new Symbol(sym.fintantque, new String(yytext()));
}

"pour" {
	return new Symbol(sym.pour, new String(yytext()));
}

"allantde" {
	return new Symbol(sym.allantde, new String(yytext()));
}

"a" {
	return new Symbol(sym.a, new String(yytext()));
}

"faire" {
	return new Symbol(sym.faire, new String(yytext()));
}

"finpour" {
	return new Symbol(sym.finpour, new String(yytext()));
}

"lire" {
	return new Symbol(sym.lire, new String(yytext()));
}

"ecrire" {
	return new Symbol(sym.ecrire, new String(yytext()));
}

"retourne" {
	return new Symbol(sym.retourne, new String(yytext()));
}

{constentier} {
	return new Symbol(sym.constentier, new String(yytext()));
}

{constchaine} {
    String s = yytext();
    s = s.substring(0, s.length() - 1) + "\n\"";
	return new Symbol(sym.constchaine, new String(s));
}

{commentaire} {
	return new Symbol(sym.commentaire, new String(yytext()));
}

{ident} {
	return new Symbol(sym.ident, new String(yytext()));
}

[\n] {;}
[\t] {;}
. {;}
