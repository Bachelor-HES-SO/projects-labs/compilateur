package arbre;

import semantique.*;

public class Modulo extends Arithmetique{
    public Modulo(int lig) {
        super(null, null, lig);
    }

    public String operateur() {
        return "%";
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    } // accepter
}
