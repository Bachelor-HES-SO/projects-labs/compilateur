package arbre;

public abstract class Binaire extends Expression{
    protected Expression operandeGauche;
    protected Expression operandeDroite;

    public Binaire(Expression operandeGauche, Expression operandeDroite, int lig) {
        this.ligne = lig;
        this.operandeGauche = operandeGauche;
        this.operandeDroite = operandeDroite;
    }

    public Expression gauche() {
        return operandeGauche;
    }

    public void grefferGauche(Expression exp) {
        this.operandeGauche = exp;
    }

    public Expression droite() {
        return operandeDroite;
    }

    public void grefferDroite(Expression exp) {
        this.operandeDroite = exp;
    }

    public String toString() {
        return "(" + operandeGauche + " " + operateur() + " " + operandeDroite + ")";
    }

    public abstract String operateur();
}
