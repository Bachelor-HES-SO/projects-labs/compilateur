package arbre;
import semantique.*;

public class InfEgal extends Relation{
    public InfEgal(int lig) {
        super(null, null, lig);
    }

    public String operateur() {
        return "<=";
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    } // accepter
}
