package arbre;

import arbre.*;
import semantique.*;

public class Lire extends Instruction{
    private Idf ident;

    public Lire (Idf e, int ligne) {
        this.ligne = ligne;
        this.ident = e;
    }

    public Idf ident() {
        return ident;
    }

    public String toString() {
        return "Lire :\tVariable : " + ident;
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    }
}
