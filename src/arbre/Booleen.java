package arbre;

import semantique.*;

public class Booleen extends Expression{
    private int valeur;

    public Booleen (int val, int lig) {
        this.ligne = lig;
        this.valeur = val;
    }

    public int valeur() {
        return valeur;
    }

    public String toString() {
        return String.valueOf(valeur);
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    } // accepter
}
