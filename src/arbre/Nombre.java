package arbre;
import semantique.*;

public class Nombre extends Expression {
    private int valeur;

    public Nombre (Integer val, int lig) {
        this.ligne = lig;
        this.valeur = val.intValue();
    }

    public int valeur() {
        return valeur;
    }

    public String toString() {
        return String.valueOf(valeur);
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    }
}
