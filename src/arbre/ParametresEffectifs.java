package arbre;

import java.util.*;
import semantique.*;

public class ParametresEffectifs extends ArbreAbstrait{
    private ArrayList<Expression> lParam;

    public ParametresEffectifs() {
        lParam = new ArrayList<Expression>();
    }

    public void addParam(Expression i) {
        lParam.add(i);
    }

    public ArrayList<Expression> listeParam() {
        return lParam;
    }

    public String toString() {
        String ch = "";
        for (Expression e : lParam) {
            ch += " ** " + e;
        }
        return ch;
    }

    public Object accepter(IVisiteur v) {
        for(Expression param: lParam) {
            v.visiter(param);
        }
        return null;
    }
}
