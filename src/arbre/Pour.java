package arbre;

import java.util.*;
import semantique.*;

public class Pour extends Instruction{
    private Idf compteur;
    private Expression borneInf;
    private Expression borneSup;
    private Bloc corps;

    public Pour(Idf compteur, Expression borneInf, Expression borneSup, Bloc corps) {
        this.compteur = compteur;
        this.borneInf = borneInf;
        this.borneSup = borneSup;
        this.corps = corps;
    }

    public Idf compteur() {
        return compteur;
    }

    public Expression borneInf() {
        return borneInf;
    }

    public Expression borneSup() {
        return borneSup;
    }

    public Bloc corps() {
        return corps;
    }

    public String toString() {
        return "Pour " + compteur + " allant de " + borneInf + " à " + borneSup + " faire\n" + corps + "\nfinpour";
    }

    public Object accepter(IVisiteur v) {
        v.visiter(this);
        return null;
    } // accepter
}
