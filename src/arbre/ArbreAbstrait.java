package arbre;

import semantique.*;

public abstract class ArbreAbstrait {
    protected int ligne;

    public int ligne() {
        return ligne;
    }
    
    public abstract Object accepter(IVisiteur v);
}
