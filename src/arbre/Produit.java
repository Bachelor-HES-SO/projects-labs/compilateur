package arbre;
import semantique.*;

public class Produit extends Arithmetique{
    public Produit(int lig) {
        super(null, null, lig);
    }

    public String operateur() {
        return "*";
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    } // accepter
}
