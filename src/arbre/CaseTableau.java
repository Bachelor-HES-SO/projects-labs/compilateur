package arbre;
import semantique.*;

public class CaseTableau extends Expression{
    private Idf ident;
    private Expression indice;

    public CaseTableau (Idf ident, Expression indice, int lig) {
        this.ligne = lig;
        this.ident = ident;
        this.indice = indice;
    }

    public Idf ident() {
        return ident;
    }

    public Expression indice() {
        return indice;
    }

    public String toString() {
        return ident + "[" + indice + "]";
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    }
}
