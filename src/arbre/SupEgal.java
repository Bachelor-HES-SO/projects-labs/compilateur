package arbre;
import semantique.*;

public class SupEgal extends Relation{
    public SupEgal(int lig) {
        super(null, null, lig);
    }

    public String operateur() {
        return ">=";
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    } // accepter
}
