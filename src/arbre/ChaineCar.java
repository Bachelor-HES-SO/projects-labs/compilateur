package arbre;
import semantique.*;

public class ChaineCar extends Expression {
    private String valeur;

    public ChaineCar (String val, int lig) {
        this.ligne = lig;
        this.valeur = val;
    }

    public String valeur() {
        return valeur;
    }

    public String toString() {
        return String.valueOf(valeur);
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    }
}
