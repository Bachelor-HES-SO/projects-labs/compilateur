package arbre;
import semantique.*;

public class Inferieur extends Relation{
    public Inferieur(int lig) {
        super(null, null, lig);
    }

    public String operateur() {
        return "<";
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    } // accepter
}
