package arbre;
import semantique.*;

public class Division extends Arithmetique{
    public Division(int lig) {
        super(null, null, lig);
    }

    public String operateur() {
        return "/";
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    } // accepter
}
