package arbre;

import semantique.*;

public class Addition extends Arithmetique{
    public Addition(int lig) {
        super(null, null, lig);
    }

    public String operateur() {
        return "+";
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    } // accepter
}
