package arbre;

import java.util.*;
import semantique.*;

public class Bloc extends Instruction{
    private ArrayList<Instruction> lInst;

    public Bloc() {
        lInst = new ArrayList<Instruction>();
    }

    public void addInst(Instruction i) {
        lInst.add(i);
    }

    public ArrayList<Instruction> listeInstr() {
        return lInst;
    }

    public String toString() {
        String ch = "";
        for (Instruction e : lInst) {
            ch += "\n" + e;
        }
        return ch;
    }

    public Object accepter(IVisiteur v) {
        for(Instruction instr: lInst) {
            v.visiter(instr);
        }
        return null;
    }
}
