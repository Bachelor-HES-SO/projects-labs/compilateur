package arbre;

import java.util.*;
import semantique.*;
import tds.entree.*;
import tds.symbole.*;

public class ProgPrincipal extends ArbreAbstrait{
    private String nom;
    private HashMap<Entree,Symbole> varsLocals;
    private HashMap<Entree,Symbole> foncsLocals;
	private HashMap<Entree, Symbole> constantes;
    private Bloc corps;
    private ListeFonctions listeFonc;

    public ProgPrincipal(String i, HashMap<Entree,Symbole> v, HashMap<Entree,Symbole> f, HashMap<Entree,Symbole> ct, ListeFonctions lf, Bloc c) {
        corps = c;
        nom = i;
        varsLocals = v;
        foncsLocals = f;
        listeFonc = lf;
        constantes = ct;
    }

    public ListeFonctions listeFonc() {
        return listeFonc;
    }

    public Bloc corps() {
        return corps;
    }

    public String nom() {
        return nom;
    }

    public HashMap<Entree,Symbole> varsLocals() {
        return varsLocals;
    }

    public HashMap<Entree,Symbole> foncsLocals() {
        return foncsLocals;
    }

    public HashMap<Entree,Symbole> constantes() {
        return constantes;
    }

    public String toString() {
        String ch = "Programme Principal : " + nom + "\n";
        if (constantes.size() != 0) {
            ch += "constantes : " + constantes + "\n";
        }
        if (varsLocals != null)
            ch += "Nombre de variables : " + varsLocals.size() + "\n";
        if (listeFonc != null)
            ch += listeFonc.toString() + "\n";
        ch += "Debut Programme\n";
        if (corps != null)
            ch += corps.toString() + "\n";
        ch += "Fin Programme";
        return ch;
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    }
}
