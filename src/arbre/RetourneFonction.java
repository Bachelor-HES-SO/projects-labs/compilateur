package arbre;

import semantique.*;
import tds.types.*;

public class RetourneFonction extends Instruction{
    private Expression expr;
    private TypeFonction type;

    public RetourneFonction(Expression e, TypeFonction t, int l) {
        expr = e;
        ligne = l;
        type = t;
    }

    public TypeFonction type() {
        return type;
    }

    public Expression expr() {
        return expr;
    }

    public String toString() {
        return "Retourne : " + expr;
    }

    public Object accepter(IVisiteur v) {
        v.visiter(this);
        return null;
    }
}
