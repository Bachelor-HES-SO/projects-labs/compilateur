package arbre;

import arbre.*;
import semantique.*;
import tds.types.*;

public class Ecrire extends Instruction{
    private Expression expr;

    public Ecrire (Expression e, int ligne) {
        this.ligne = ligne;
        this.expr = e;
    }

    public Expression expr() {
        return expr;
    }

    public String toString() {
        return "Ecrire :\tExpression : " + expr;
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    }
}
