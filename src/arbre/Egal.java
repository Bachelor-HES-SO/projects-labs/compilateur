package arbre;
import semantique.*;

public class Egal extends Relation{
    public Egal(int lig) {
        super(null, null, lig);
    }

    public String operateur() {
        return "==";
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    } // accepter
}
