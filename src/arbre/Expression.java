package arbre;
import tds.types.*;

public abstract class Expression extends ArbreAbstrait{
    protected Type type;

    public Type type() {
        return type;
    }

    public void setType(Type t) {
        type = t;
    }
}
