package arbre;

import arbre.*;
import semantique.*;
import tds.types.*;

public class Affectation extends Instruction{
    private Expression source;
    private Expression dest;

    public Affectation (Expression source, Expression dest, int ligne) {
        this.source = source;
        this.dest = dest;
        this.ligne = ligne;
    }

    public Expression destination() {
        return dest;
    }

    public Expression source() {
        return source;
    }

    public void setSource(Expression e) {
        source = e;
    }

    public String toString() {
        return "Affectation :\tsource : " + source + ", dest : " + dest;
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    }
}
