package arbre;

import java.util.*;
import semantique.*;

public class Condition extends Instruction{
    private Expression condition;
    private Bloc alors;
    private Bloc sinon;

    public Condition(Expression condition,Bloc alors, Bloc sinon) {
        this.condition = condition;
        this.alors = alors;
        this.sinon = sinon;
    }

    public Expression condition() {
        return condition;
    }

    public Bloc alors() {
        return alors;
    }

    public Bloc sinon() {
        return sinon;
    }

    public String toString() {
        return "si : " + condition + " alors " + alors + " sinon " + sinon;
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    } // accepter
}
