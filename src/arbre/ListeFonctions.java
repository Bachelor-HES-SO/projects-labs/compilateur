package arbre;

import java.util.*;
import semantique.*;

public class ListeFonctions extends Instruction{
    private ArrayList<Fonction> lFonc;

    public ListeFonctions() {
        lFonc = new ArrayList<Fonction>();
    }

    public void addFonc(Fonction i) {
        lFonc.add(i);
    }

    public ArrayList<Fonction> listeFonc() {
        return lFonc;
    }

    public String toString() {
        String ch = "";
        for (Fonction e : lFonc) {
            ch += "\n" + e;
        }
        return ch;
    }

    public Object accepter(IVisiteur v) {
        for(Fonction fonc: lFonc) {
            v.visiter(fonc);
        }
        return null;
    }
}
