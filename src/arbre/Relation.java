package arbre;

public abstract class Relation extends Binaire{
    public Relation(Expression operandeGauche, Expression operandeDroite, int lig) {
        super(operandeGauche, operandeDroite, lig);
    }
}