package arbre;

import java.util.*;
import semantique.*;

public class TantQue extends Instruction{
    private Expression condition;
    private Bloc corps;

    public TantQue(Expression condition, Bloc corps) {
        this.condition = condition;
        this.corps = corps;
    }

    public Expression condition() {
        return condition;
    }

    public Bloc corps() {
        return corps;
    }

    public String toString() {
        return "tantque : " + condition + " corps " + corps;
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    } // accepter
}
