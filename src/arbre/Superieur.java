package arbre;
import semantique.*;

public class Superieur extends Relation{
    public Superieur(int lig) {
        super(null, null, lig);
    }

    public String operateur() {
        return ">";
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    } // accepter
}
