package arbre;

import java.util.*;
import semantique.*;
import tds.entree.*;
import tds.symbole.*;

public class Fonction extends ArbreAbstrait{
    private String nom;
    private HashMap<Entree,Symbole> varsLocals;
    private HashMap<Entree,Symbole> paramsFonc;
    private String typeRetour;
    private ArrayList<String> typesParams;
    private Bloc corps;

    public Fonction(String i, HashMap<Entree,Symbole> p, HashMap<Entree,Symbole> v, Bloc c) {
        corps = c;
        nom = i;
        if (v != null)
            varsLocals = v;
        else
            varsLocals = new HashMap<Entree,Symbole>();
        if (p != null)
            paramsFonc = p;
        else
            paramsFonc = new HashMap<Entree,Symbole>();
        typesParams = new ArrayList<String>();
    }

    public ArrayList<String> typesParams() {
        return typesParams;
    }

    public void addTypeParam(int i, String p) {
        typesParams.add(i, p);
    }

    public String typeRetour() {
        return typeRetour;
    }

    public void setTypeRetour(String t) {
        typeRetour = t;
    }

    public Bloc corps() {
        return corps;
    }

    public String nom() {
        return nom;
    }

    public HashMap<Entree,Symbole> varsLocals() {
        return varsLocals;
    }

    public HashMap<Entree,Symbole> paramsFonc() {
        return paramsFonc;
    }

    public String toString() {
        String ch = "Fonction : " + nom + "\nNombre de variables : " + varsLocals.size() +
            "\nDebut fonction\n";
        if (corps != null)
            ch += corps.toString() + "\n";
        ch += "Fin Fonction\n";
        return ch;
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    }
}
