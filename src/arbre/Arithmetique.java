package arbre;

public abstract class Arithmetique extends Binaire{
    public Arithmetique(Expression operandeGauche, Expression operandeDroite, int lig) {
        super(operandeGauche, operandeDroite, lig);
    }
}
