package arbre;

import java.util.*;
import semantique.*;
import tds.types.*;

public class AppelQualifie extends Expression{
    private Idf ident;
    private ParametresEffectifs lParam;
    private String typeRetour;
    private ArrayList<String> typesParams;

    public AppelQualifie(Idf i, ParametresEffectifs lp) {
        lParam = lp;
        ident = i;
        typesParams = new ArrayList<String>();
    }

    public ParametresEffectifs listeParam() {
        return lParam;
    }

    public ArrayList<String> typesParams() {
        return typesParams;
    }

    public void addTypeParam(int i, String p) {
        typesParams.add(i, p);
    }

    public Idf ident() {
        return ident;
    }

    public String typeRetour() {
        return typeRetour;
    }

    public void setTypeRetour(String t) {
        typeRetour = t;
    }

    public String toString() {
        return "Appel fonction : " + ident + " ; Param effectifs : " + lParam;
    }

    public boolean paramsValides(ArrayList<Type> l) {
        if (l.size() != lParam.listeParam().size()) {
            return false;
        }
        for (int i = 0; i < l.size(); i++) {
            String type1 = l.get(i).getClass().toString();
            String type2 = lParam.listeParam().get(i).type().getClass().toString();
            if (!type1.equals(type2)) {
                return false;
            }
        }
        return true;
    }

    public Object accepter(IVisiteur v) {
        v.visiter(this);
        return null;
    }
}
