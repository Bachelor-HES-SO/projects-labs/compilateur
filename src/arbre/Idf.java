package arbre;
import semantique.*;
import tds.types.*;

public class Idf extends Expression{
    private String nom;

    public Idf (String nom, int lig) {
        this.ligne = lig;
        this.nom = nom;
    }

    public String nom() {
        return nom;
    }

    public String toString() {
        return nom;
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    }
}
