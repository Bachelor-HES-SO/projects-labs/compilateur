package arbre;
import semantique.*;

public class Soustraction extends Arithmetique{
    public Soustraction(int lig) {
        super(null, null, lig);
    }

    public String operateur() {
        return "-";
    }

    public Object accepter(IVisiteur v) {
        return v.visiter(this);
    } // accepter
}
