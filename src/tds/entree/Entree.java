package tds.entree;

public class Entree {
    protected Ident ident;
	// construire une entrée
	public Entree(Ident ident) {
        this.ident = ident;
	}
	// Identificateur attachée à l’entrée
    public Ident ident() {
        return this.ident;
    }

    @Override
    public String toString() {
        return this.ident.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Entree)) {
            return false;
        }
        Entree e = (Entree)obj;
        return ident.nom().equals(e.ident.nom());
    }

    @Override
    public int hashCode() {
        return ident.nom().hashCode();
    }
} // class Entree
