package tds.entree;

public class Ident {
    private String nom;

    public Ident(String nom) {
        this.nom = nom;
    }

    public String nom() {
        return this.nom;
    }

    @Override
    public String toString() {
        return this.nom;
    }
}
