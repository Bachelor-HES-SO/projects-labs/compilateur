package tds;

import java.util.*;
import tds.entree.*;
import tds.symbole.*;
import tds.types.*;
import semantique.*;

public class TDS {
	//champ privés
	private Stack<Integer> pile;
	private HashMap<Entree, Stack<Association>> dico;
 	private int numeroBloc = -1;
	private HashMap<Entree, Symbole> varsLocals;
	private Stack<HashMap<Entree, Symbole>> pileVarsLocals;
	private HashMap<Entree, Symbole> foncsLocals;
	private HashMap<Entree, Symbole> paramsFonc;
	private HashMap<Entree, Symbole> constantes;
	private int indiceVar;
	private int nombreParam;

	private static TDS instance = new TDS();

	// consultation du singleton
	public static TDS getInstance() {
		return instance;
	}

	private TDS() {
		pile = new Stack<Integer>();
		dico = new HashMap<Entree, Stack<Association>>();
		varsLocals = new HashMap<Entree, Symbole>();
		pileVarsLocals = new Stack<HashMap<Entree, Symbole>>();
		foncsLocals = new HashMap<Entree, Symbole>();
		paramsFonc = new HashMap<Entree, Symbole>();
		constantes = new HashMap<Entree, Symbole>();
		indiceVar = 0;
	}

	// entrer dans un nouveau bloc
	public void entreeBloc() {
		numeroBloc++;
		pile.push(new Integer(numeroBloc));
		if (numeroBloc != 0) {
			pileVarsLocals.push((HashMap<Entree, Symbole>)varsLocals.clone());
			varsLocals.clear();
			indiceVar = 0;
			paramsFonc.clear();
		}
	}
	// sortir du bloc courant
	public void sortieBloc() {
		pile.pop();
		numeroBloc--;
		if (pileVarsLocals.size() != 0) {
			varsLocals = pileVarsLocals.pop();
		}
	}
	// ajouter un couple entrée, symbole
	public void ajouter(Entree e, Symbole s) {
		Stack<Association> ls = dico.get(e);
		if (ls == null) {
			//nouvelle entrée, on crée une pile vide
			ls = new Stack<Association>();
			ls.push(new Association(s, numeroBloc));
			dico.put(e, ls);
			if (s.type() instanceof TypeProgramme || s.type() instanceof TypeFonction) {
				foncsLocals.put(e, s);
			} else {
				if (s instanceof SymboleConstEnt) {
					constantes.put(e, s);
				} else {
					s.setIndiceVar(indiceVar);
					varsLocals.put(e, s);
					indiceVar++;
				}
			}
		} else {
			Association premier = ls.peek();
			if (premier.numeroBloc() == numeroBloc) {
				// System.out.println("double déclaration");
				FlotErreurs.getInstance().push(ErreurSemantique.doubleDecl(e, s.ligne()));
				// FlotErreurs.getInstance().push(s.ligne(), "double déclaration");
			} else {
				//adjonction dans une liste existante
				ls.push(new Association(s, numeroBloc));
				if (s.type() instanceof TypeProgramme || s.type() instanceof TypeFonction) {
					foncsLocals.put(e, s);
				} else {
					if (s instanceof SymboleConstEnt) {
						constantes.put(e, s);
					} else {
						s.setIndiceVar(indiceVar);
						varsLocals.put(e, s);
						indiceVar++;
					}
				}
			}
		}
	}
	// identifier une entrée dans un bloc ouvert
	public Symbole identifier(Entree e) {
		Stack<Association> ls = dico.get(e);
		if (ls == null) return null; //indefini
		int indicePile = pile.size() - 1;
		int indiceLs = ls.size() - 1;
		boolean fin = false;
		Symbole s = null;
		int premListe, premPile;
		Association assoc;
		while (!fin && indicePile != -1 && indiceLs != -1) {
			assoc = ls.get(indiceLs);
			premListe = assoc.numeroBloc();
			premPile = pile.get(indicePile).intValue();
			if (premListe == premPile) { //symbole trouvé
				s = assoc.symbole();
				fin = true;
			} else if (premListe < premPile) {
				indiceLs--;
			} else {
				indicePile--;
			}
		}
		return s;
	}

	public ArrayList<Type> getTypesParams() {
		ArrayList<Type> l = new ArrayList<Type>();
		for (Map.Entry<Entree, Symbole> cle : paramsFonc.entrySet()) {
			l.add(cle.getValue().indiceVar(), cle.getValue().type());
		}
		return l;
	}

	public HashMap<Entree, Symbole> varsLocals() {
		return varsLocals;
	}

	public HashMap<Entree, Symbole> foncsLocals() {
		return foncsLocals;
	}

	public HashMap<Entree, Symbole> paramsFonc() {
		return paramsFonc;
	}

	public HashMap<Entree, Symbole> constantes() {
		return constantes;
	}

	public void addParam(Entree e, Symbole s) {
		paramsFonc.put(e, s);
	}

	public void affichage() {
		Set<Map.Entry<Entree, Stack<Association>>> set = dico.entrySet();
		Iterator<Map.Entry<Entree, Stack<Association>>> iterator = set.iterator();
		while(iterator.hasNext()) {
			Map.Entry<Entree, Stack<Association>> mentry = iterator.next();
			System.out.println("key is: "+ mentry.getKey() + " & Value is: " + mentry.getValue());
		}
	}
}
