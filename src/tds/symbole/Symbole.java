package tds.symbole;

import tds.types.*;

public class Symbole {
	protected Type type;
	protected int ligne;
    protected int indiceVar;
	// construire un Symbole
	protected Symbole(int ligne, Type type) {
		this.ligne = ligne;
		this.type = type;
	}
	// Ligne dans le texte

	public Type type() {
		return type;
	}

	public int ligne() {
		return this.ligne;
	}

    public int indiceVar() {
        return indiceVar;
    }

    public void setIndiceVar(int i) {
        indiceVar = i;
    }

	@Override
	public String toString() {
		return "Ligne : " + ligne + " - type : " + type + " - indice " + indiceVar;
	}
} // class Symbole
