package tds.symbole;

import tds.types.*;
import arbre.*;

public class SymboleConstEnt extends Symbole {
    private Expression expr;

    public SymboleConstEnt(int ligne, Type type, Expression ex) {
        super(ligne, type);
        expr = ex;
    }

    public Expression expr() {
        return expr;
    }

    public String toString() {
        return super.toString() + " - expression : " + expr;
    }
}
