package tds.types;

public class TypeProgramme extends Type {
    private static TypeProgramme instance = new TypeProgramme();

    // consultation du singleton
    public static TypeProgramme getInstance() {
        return instance;
    }

    public boolean estConforme(Type other) {
        return other instanceof TypeProgramme;
    }
}
