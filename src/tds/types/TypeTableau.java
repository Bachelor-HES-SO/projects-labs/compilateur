package tds.types;

public class TypeTableau extends Type {
    protected Type type;        //type des éléments
    protected int[] borneInf;   //bornes inférieures
    protected int[] borneSup;   //bornes supérieures

    //construire un symbole de TypeTableau
    public TypeTableau(Type t, int[] binf, int[] bsup) {
        this.type = t;
        this.borneInf = binf;
        this.borneSup = bsup;
    }

    //type des éléments
    public Type type() {
        return type;
    }

    //nombre de dimensions
    public int nbDimensions() {
        return borneSup.length;
    }

    //borne inférieure de la kème dimension
    public int borneInferieure(int k) {
        return borneInf[k];
    }

    //borne supérieure de la kième dimension
    public int borneSuperieur(int k) {
        return borneSup[k];
    }

    //redéfinition de toString
    public String toString() {
        String ch = "tableau de " + type + "[";
        for (int i = 0; i < nbDimensions() - 1; i++) {
            ch += borneInf[i] + ".." + borneSup[i] + " , ";
        }
        ch += borneInf[nbDimensions() - 1] + ".." + borneSup[nbDimensions() - 1] + "]";
        ch += "dim = " + nbDimensions();
        return ch;
    }

    //test de conformité
    public boolean estConforme(Type other) {
        if (!(other instanceof TypeTableau)) return false;
        int nb, nbto;
        TypeTableau to = (TypeTableau) other;
        boolean res = (nbDimensions() == to.nbDimensions()) && type().estConforme(to.type());
        for (int k = 0; k < nbDimensions() && res; k++) {
            nb = borneSuperieur(k) - borneInferieure(k);
            nbto = to.borneSuperieur(k) - to.borneInferieure(k);
            res = nb == nbto;
        }
        return res;
    }
}
