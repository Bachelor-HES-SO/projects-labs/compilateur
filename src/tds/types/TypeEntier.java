package tds.types;

public class TypeEntier extends Type {
    private static TypeEntier instance = new TypeEntier();

    // consultation du singleton
    public static TypeEntier getInstance() {
        return instance;
    }
    public boolean estConforme(Type other) {
        return other instanceof TypeEntier;
    }
}
