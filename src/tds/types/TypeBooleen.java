package tds.types;

public class TypeBooleen extends Type {
    private static TypeBooleen instance = new TypeBooleen();

    // consultation du singleton
    public static TypeBooleen getInstance() {
        return instance;
    }

    public boolean estConforme(Type other) {
        return other instanceof TypeBooleen;
    }
}
