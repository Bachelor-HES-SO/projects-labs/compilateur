package tds.types;

import java.util.*;

public class TypeFonction extends Type {
    private Type typeRetour;
    private ArrayList<Type> typesParams;

    public TypeFonction(Type t) {
        typeRetour = t;
        typesParams = new ArrayList<Type>();
    }

    public Type typeRetour() {
        return typeRetour;
    }

    public ArrayList<Type> typesParams() {
        return typesParams;
    }

    public void setTypesParams(ArrayList<Type> l) {
        typesParams = (ArrayList<Type>) l.clone();
    }

    public boolean estConforme(Type other) {
        return other instanceof TypeFonction;
    }
}
