package tds.types;

public abstract class Type {
	/** Conformite de 2 types
	* @param other un autre type
	* @return vrai si this est conforme a other
	*/
	public abstract boolean estConforme(Type other);

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
