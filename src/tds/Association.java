package tds;

import tds.symbole.*;

public class Association {
    private Symbole symbole;
    private int numeroBloc;

    public Association(Symbole symbole, int numeroBloc) {
        this.symbole = symbole;
        this.numeroBloc = numeroBloc;
    }

    public Symbole symbole() {
        return symbole;
    }

    public int numeroBloc() {
        return numeroBloc;
    }

    public String toString() {
        return "numBloc = " + numeroBloc + "\tSymbole : " + symbole;
    }
}
