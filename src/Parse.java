/**
 * Analyseur SGML
 * Steven Liatti
 * ITI2 - HEPIA - 2016
 */

import java.util.*;
import java.io.FileReader;

public class Parse {
	public static void main (String[] arg) {
		try {
			FileReader myFile = new FileReader(arg[0]);
			Lexer myLex = new Lexer(myFile);
			parser myP = new parser(myLex);
			try { myP.parse(); }
			catch (Exception e) {
				e.printStackTrace();
				System.out.println("parse error");
			}
		}
		catch (Exception e) {
			System.out.println("invalid file");
		}
	}
}
