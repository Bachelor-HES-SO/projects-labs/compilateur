package semantique;

import java.util.*;
import arbre.*;
import tds.symbole.*;
import tds.entree.*;
import tds.types.*;
import tds.*;


public class AnalyseurSemantique implements IVisiteur {
	private HashMap<Entree, Symbole> listeVarsCourants;
	private HashMap<Entree, Symbole> listeFoncsCourants;
	private HashMap<Entree, Symbole> listeConstantes;
	private static AnalyseurSemantique instance = new AnalyseurSemantique();

    private AnalyseurSemantique() {}

    // consultation du singleton
    public static AnalyseurSemantique getInstance() {
        return instance;
    }

	public Object visiter(ProgPrincipal p) {
		listeVarsCourants = p.varsLocals();
		listeFoncsCourants = p.foncsLocals();
		listeConstantes = p.constantes();
		if (p.listeFonc() != null)
			p.listeFonc().accepter(this);
		listeVarsCourants = p.varsLocals();
		listeFoncsCourants = p.foncsLocals();
		if (p.corps() != null)
			p.corps().accepter(this);
		return null;
	}

	public Object visiter(Fonction f) {
		Entree e = new Entree(new Ident(f.nom()));
		Symbole s = listeFoncsCourants.get(e);
		if (s != null && s.type() instanceof TypeFonction) {
			Type t = ((TypeFonction) s.type()).typeRetour();
			if (t instanceof TypeEntier || t instanceof TypeBooleen) {
				f.setTypeRetour("I");
			}
			if (t instanceof TypeTableau) {
				f.setTypeRetour("[I");
			}
		}
		if (f.paramsFonc().size() != 0) {
			for (Map.Entry<Entree, Symbole> cle : f.paramsFonc().entrySet()) {
				Symbole ss = cle.getValue();
				if (ss.type() instanceof TypeTableau) {
					f.addTypeParam(ss.indiceVar(), "[I");
				} else {
					f.addTypeParam(ss.indiceVar(), "I");
				}
			}
		}
		listeVarsCourants = f.varsLocals();
		if (f.corps() != null)
			f.corps().accepter(this);
		return null;
	}

    public Object visiter (RetourneFonction r) {
		r.expr().accepter(this);
		Type t = r.expr().type();
		if (!t.estConforme(r.type().typeRetour())) {
			FlotErreurs.getInstance().push(ErreurSemantique.typeRetourNonConforme(r.type().typeRetour(), t, r.ligne()));
		}
		return null;
	}

	public Object visiter(AppelQualifie a) {
		a.ident().accepter(this);
		Entree e = new Entree(new Ident(a.ident().nom()));
		Symbole s = listeFoncsCourants.get(e);
		if (s != null) {
			if (!(a.ident().type() instanceof TypeFonction)) {
	            FlotErreurs.getInstance().push(ErreurSemantique.typeNonFonction(a.ident().nom(), a.ident().type(), a.ligne()));
			} else {
				TypeFonction typef = (TypeFonction) a.ident().type();
				Type t = typef.typeRetour();
				a.setType(t);
				if (t instanceof TypeEntier || t instanceof TypeBooleen) {
					a.setTypeRetour("I");
				}
				if (t instanceof TypeTableau) {
					a.setTypeRetour("[I");
				}
				if (a.listeParam() != null) {
					a.listeParam().accepter(this);
					if (!a.paramsValides(typef.typesParams())) {
						FlotErreurs.getInstance().push(ErreurSemantique.typeParamsNonConformes(a.ident().nom(), a.listeParam(), a.ident().ligne()));
					} else {
						ArrayList<Expression> liste = a.listeParam().listeParam();
						for (int i = 0; i < liste.size(); i++) {
							if (liste.get(i).type() instanceof TypeTableau) {
								a.addTypeParam(i, "[I");
							} else {
								a.addTypeParam(i, "I");
							}
						}
					}
				}

			}
		}
		return null;
	}

    public Object visiter(Arithmetique a) {
        Object valG = a.gauche().accepter(this);
		Type typeGauche = a.gauche().type();
		if (typeGauche == null) return null;
        Object valD = a.droite().accepter(this);
		Type typeDroite = a.droite().type();
		if (typeDroite == null) return null;
        if (!(typeGauche.estConforme(typeDroite)) || !(typeGauche instanceof TypeEntier))
            FlotErreurs.getInstance().push(ErreurSemantique.typeNonConformesBinaire(typeGauche, typeDroite, a.ligne()));
		else {
			a.setType(typeDroite);
		}
        return null;
    }

    public Object visiter(Relation a) {
		Object valG = a.gauche().accepter(this);
		Type typeGauche = a.gauche().type();
		if (typeGauche == null) return null;
        Object valD = a.droite().accepter(this);
		Type typeDroite = a.droite().type();
		if (typeDroite == null) return null;
        if (!(typeGauche.estConforme(typeDroite)) || (!(typeGauche instanceof TypeEntier) && !(typeGauche instanceof TypeBooleen)))
            FlotErreurs.getInstance().push(ErreurSemantique.typeNonConformesBinaire(typeGauche, typeDroite, a.ligne()));
		else {
			a.setType(TypeBooleen.getInstance());
		}
		return null;
	}

    public Object visiter(Affectation a) {
        a.destination().accepter(this);
        Type typeDest = a.destination().type();
		if (typeDest == null) return null;
        a.source().accepter(this);
        Type typeSource = a.source().type();
		if (typeSource == null) return null;
        if (!(typeSource.estConforme(typeDest)) )
            FlotErreurs.getInstance().push(ErreurSemantique.typeNonConformesAffectation(typeDest, typeSource, a.ligne()));
        return null;
    }

	public Object visiter(Condition c) {
		c.condition().accepter(this);
		if (c.condition().type() != null) {
			if (c.condition().type() != TypeBooleen.getInstance())
				FlotErreurs.getInstance().push(ErreurSemantique.booleenAttendu(c.condition().ligne()));
		}
		c.alors().accepter(this);
		if (c.sinon() != null)
			c.sinon().accepter(this);
		return null;
	}

    public Object visiter(TantQue t) {
		t.condition().accepter(this);
		if (t.condition().type() != null) {
			if (t.condition().type() != TypeBooleen.getInstance())
				FlotErreurs.getInstance().push(ErreurSemantique.booleenAttendu(t.condition().ligne()));
		}
		t.corps().accepter(this);
		return null;
	}

    public Object visiter(Pour p) {
		p.compteur().accepter(this);
		if (p.compteur().type() != null && p.compteur().type() != TypeEntier.getInstance()) {
			FlotErreurs.getInstance().push(ErreurSemantique.entierAttenduPour(p.compteur().type(), p.compteur().ligne()));
		}
		p.borneInf().accepter(this);
		if (p.borneInf().type() != null && p.borneInf().type() != TypeEntier.getInstance()) {
			FlotErreurs.getInstance().push(ErreurSemantique.entierAttenduBorne(p.borneInf().type(), p.borneInf().ligne()));
		}
		p.borneSup().accepter(this);
		if (p.borneSup().type() != null && p.borneSup().type() != TypeEntier.getInstance()) {
			FlotErreurs.getInstance().push(ErreurSemantique.entierAttenduBorne(p.borneSup().type(), p.borneSup().ligne()));
		}
		p.corps().accepter(this);
		return null;
	}

    public Object visiter (Nombre n) {
		n.setType(TypeEntier.getInstance());
		return n.valeur();
    }

    public Object visiter (Booleen n) {
		n.setType(TypeBooleen.getInstance());
		return n.valeur();
    }

    public Object visiter (ChaineCar s) {
		return null;
	}

    public Object visiter (Idf i) {
		Entree e = new EntreeEntBool(new Ident(i.nom()));
		Symbole s = listeVarsCourants.get(e);
		if (s == null) {
			s = listeFoncsCourants.get(e);
			if (s == null)
				s = listeConstantes.get(e);
				if (s == null)
					FlotErreurs.getInstance().push(ErreurSemantique.identInconnu(e, i.ligne()));
			else {
				i.setType(s.type());
			}
		}
		else {
			i.setType(s.type());
		}
        return null;
    }

    public Object visiter (CaseTableau c) {
		c.ident().accepter(this);
		Type t = c.ident().type();
		if (!(t instanceof TypeTableau)) {
			FlotErreurs.getInstance().push(ErreurSemantique.typeNonIndexe(t, c.ligne()));
		} else {
			c.indice().accepter(this);
			t = c.indice().type();
			if (t != TypeEntier.getInstance()) {
				FlotErreurs.getInstance().push(ErreurSemantique.typeNonConformeIndice(t, c.ligne()));
			} else {
				if (t instanceof TypeTableau) {
					c.setType(((TypeTableau)t).type());
				} else {
					c.setType(t);
				}
			}
		}
		return null;
	}

    public Object visiter (Instruction i) {
		i.accepter(this);
		return null;
    }

    public Object visiter (Expression i) {
		i.accepter(this);
		return null;
    }

    public Object visiter (Ecrire b) {
		b.expr().accepter(this);
		return null;
	}

	public Object visiter(Lire l) {
		l.ident().accepter(this);
		Type t = l.ident().type();
		if (!(t instanceof TypeEntier)) {
			FlotErreurs.getInstance().push(ErreurSemantique.lectureImpossible(t, l.ligne()));
		}
		return null;
	}
}
