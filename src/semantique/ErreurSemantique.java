package semantique;

import java.util.*;
import tds.entree.*;
import tds.types.*;
import arbre.*;

public class ErreurSemantique {
    public static String doubleDecl(Entree e, int ligne) {
        return "ligne " + ligne + " - Erreur sémentique : Double déclaration de la variable \"" + e.toString() + "\" ";
    }

    public static String typeNonConformesAffectation(Type typeDest, Type typeSource, int ligne) {
        return "ligne " + ligne + " - Erreur sémantique : Source et destination non conformes\n" +
            "\tExcepté : " + typeDest + "\n\tTrouvé : " + typeSource;
    }

    public static String typeNonConformesBinaire(Type typeGauche, Type typeDroite, int ligne) {
        return "ligne " + ligne + " - Erreur sémantique : Expression binaire non permise entre les types " +
            typeGauche + " et " + typeDroite;
    }

    public static String identInconnu(Entree e, int ligne) {
        return "ligne " + ligne + " - Erreur sémantique : Identificateur inconnu " + e;
    }

    public static String booleenAttendu(int ligne) {
        return "ligne " + ligne + " - Erreur sémantique : Expression booléenne attendue";
    }

    public static String entierAttenduPour(Type typeTrouve, int ligne) {
        return "ligne " + ligne + " - Erreur sémantique : Le compteur doit être de TypeEntier\n" +
            "Trouvé : " + typeTrouve;
    }

    public static String entierAttenduBorne(Type typeTrouve, int ligne) {
        return "ligne " + ligne + " - Erreur sémantique : Les bornes de \"pour\" doivent être de TypeEntier\n" +
            "Trouvé : " + typeTrouve;
    }

    public static String lectureImpossible(Type t, int ligne) {
        return "ligne " + ligne + " - Erreur sémantique : Impossible de lire une variable " + t;
    }

    public static String typeNonConformeIndice(Type t, int ligne) {
        return "ligne " + ligne + " - Erreur sémantique : Indice du tableau ne peut pas être " + t;
    }

    public static String typeNonIndexe(Type t, int ligne) {
        return "ligne " + ligne + " - Erreur sémantique : " + t + " ne peut pas être indéxé";
    }

    public static String typeNonFonction(String s, Type t, int ligne) {
        return "ligne " + ligne + " - Erreur sémantique : " + s + " n'est pas une fonction\n" +
            "\tExcepté : TypeFonction" + "\n\tTrouvé : " + t;
    }

    public static String typeRetourNonConforme(Type excepte, Type trouve, int ligne) {
        return "ligne " + ligne + " - Erreur sémantique : Type de retour de fonction non conforme\n" +
            "\tExcepté : " + excepte + "\n\tTrouvé : " + trouve;
    }

    public static String typeParamsNonConformes(String n, ParametresEffectifs l, int ligne) {
        String ch = "ligne " + ligne + " - Erreur sémantique : Impossible de trouver une fonction avec la signature " +
            "suivante\n\t" + n + "(";
        if (l != null) {
            int size = l.listeParam().size();
            for(int i = 0; i < size - 1; i++) {
                ch += l.listeParam().get(i).type() + ", ";
            }
            ch += l.listeParam().get(size - 1).type();
        }
        return ch + ")";
    }
}
