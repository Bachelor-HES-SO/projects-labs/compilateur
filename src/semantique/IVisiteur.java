package semantique;

import arbre.*;

public interface IVisiteur {
    public Object visiter(ProgPrincipal p);
    public Object visiter(Fonction f);
    public Object visiter(AppelQualifie a);
    public Object visiter(Arithmetique a);
    public Object visiter(Relation r);
    public Object visiter(Affectation a);
    public Object visiter(Nombre n);
    public Object visiter(ChaineCar s);
    public Object visiter(Idf i);
    public Object visiter(Booleen n);
    public Object visiter(Instruction i);
    public Object visiter(Expression b);
    public Object visiter(RetourneFonction r);
    public Object visiter(Ecrire b);
    public Object visiter(Lire l);
    public Object visiter(Condition b);
    public Object visiter(TantQue t);
    public Object visiter(Pour p);
    public Object visiter(CaseTableau c);
}
