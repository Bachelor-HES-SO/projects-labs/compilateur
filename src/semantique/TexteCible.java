package semantique;

import java.util.*;
import java.io.*;
import tds.types.*;
import tds.entree.*;
import tds.symbole.*;

public class TexteCible extends ArrayList<String>{
    private static TexteCible instance = new TexteCible();
    private int enteteNum = 0;
    private String nomProg;

    private TexteCible() {}

    public static TexteCible getInstance() {
        return instance;
    }

    public void enteteProgramme(String nomProgramme, HashMap<Entree, Symbole> consts) {
        nomProg = nomProgramme;
        add(".class public " + nomProgramme);
        add(".super java/lang/Object\n");
        enteteNum = 2;
        if (consts.size() != 0) {
			for (Map.Entry<Entree, Symbole> cle : consts.entrySet()) {
				SymboleConstEnt s = (SymboleConstEnt) cle.getValue();
				add(".field private static " + cle.getKey() + " I");
                enteteNum ++;
			}
        }
    }

    public void debutProgramme(int nombreVar) {
        add(".method public static main([Ljava/lang/String;)V ");
        add(".limit stack 100");
        add(".limit locals " + nombreVar); // le "+1" pour une variable auxiliaire
    }

    public void finFichier() {
        add("return");
        add(".end method");
    }

    public void addDebutFonction(String nomFonc, int nombreVar, ArrayList<String> params, String typeRet) {
        String typesP = "";
        for (String s : params) {
            typesP += s;
        }
        add(".method public static " + nomFonc + "(" + typesP + ")" + typeRet);
        add(".limit stack 100");
        add(".limit locals " + nombreVar); // le "+1" pour une variable auxiliaire
    }

    public void addFinFonction() {
        add(".end method\n");
    }

    public ArrayList<String> addEcrireMethod() {
        ArrayList<String> l = new ArrayList<String>();
        l.add("; ecrire entier");
        l.add(".method public static print(I)V");
        l.add(".limit locals 1");
        l.add(".limit stack 2");
        l.add("; push java.lang.System.out (type PrintStream)");
        l.add("getstatic java/lang/System/out Ljava/io/PrintStream;");
        l.add("; push entier à écrire");
        l.add("iload 0");
        l.add("; invoke println");
        l.add("invokevirtual java/io/PrintStream/println(I)V");
        l.add("return");
        l.add(".end method\n");
        return l;
    }

    public ArrayList<String> addEcrireChaineMethod() {
        ArrayList<String> l = new ArrayList<String>();
        l.add("; ecrire chaine de caractère");
        l.add(".method public static print(Ljava/lang/String;)V");
        l.add(".limit locals 1");
        l.add(".limit stack 2");
        l.add("; push java.lang.System.out (type PrintStream)");
        l.add("getstatic java/lang/System/out Ljava/io/PrintStream;");
        l.add("; push chaine à écrire");
        l.add("aload 0");
        l.add("; invoke println");
        l.add("invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V");
        l.add("return");
        l.add(".end method\n");
        return l;
    }

    public ArrayList<String> addLireMethod() {
        ArrayList<String> l = new ArrayList<String>();
        l.add("; lire entier");
        l.add(".method public static read()I");
        l.add(".limit locals 10 ");
        l.add(".limit stack 10 ");
        l.add("ldc 0 ");
        l.add("istore 1  ; this will hold our final integer ");
        l.add("Label1: ");
        l.add("getstatic java/lang/System/in Ljava/io/InputStream; ");
        l.add("invokevirtual java/io/InputStream/read()I ");
        l.add("istore 2 ");
        l.add("iload 2 ");
        l.add("ldc 10   ; the newline delimiter ");
        l.add("isub ");
        l.add("ifeq Label2 ");
        l.add("iload 2 ");
        l.add("ldc 32   ; the space delimiter ");
        l.add("isub ");
        l.add("ifeq Label2 ");
        l.add("iload 2 ");
        l.add("ldc 48   ; we have our digit in ASCII, have to subtract it from 48 ");
        l.add("isub ");
        l.add("ldc 10 ");
        l.add("iload 1 ");
        l.add("imul ");
        l.add("iadd ");
        l.add("istore 1 ");
        l.add("goto Label1 ");
        l.add("Label2: ");
        l.add(";when we come here we have our integer computed in Local Variable 1 ");
        l.add("iload 1 ");
        l.add("ireturn ");
        l.add(".end method\n");
        return l;
    }

    public int enteteNum() {
        return enteteNum;
    }

    public void genererFichierCible() {
        try {
            FileWriter f = new FileWriter(nomProg + ".j");
            for(String str: this) {
                f.write(str + "\n");
            }
            f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
