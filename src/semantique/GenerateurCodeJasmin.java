package semantique;

import java.util.*;
import arbre.*;
import tds.symbole.*;
import tds.entree.*;
import tds.*;
import tds.types.*;

public class GenerateurCodeJasmin implements IVisiteur {
	private static TexteCible cible = TexteCible.getInstance();
	private HashMap<Entree, Symbole> listeConstantes;
	private static GenerateurCodeJasmin instance = new GenerateurCodeJasmin();

	private String nomProg;
	private HashMap<Entree, Symbole> listeVarsCourants;

	private boolean ecrireEntInserree = false;
	private boolean ecrireChaineInserree = false;
	private boolean lireEntInserree = false;

    private GenerateurCodeJasmin() {}

    // consultation du singleton
    public static GenerateurCodeJasmin getInstance() {
        return instance;
    }

	public Object visiter(ProgPrincipal p) {
		listeConstantes = p.constantes();
		listeVarsCourants = p.varsLocals();
		nomProg = p.nom();
		cible.enteteProgramme(nomProg, p.constantes());
		if (p.constantes().size() != 0) {
			cible.add(".method static <clinit>()V");
			cible.add(".limit stack 1");
			cible.add(".limit locals 0");
			for (Map.Entry<Entree, Symbole> cle : p.constantes().entrySet()) {
				SymboleConstEnt s = (SymboleConstEnt) cle.getValue();
				s.expr().accepter(this);
				cible.add("putstatic " + nomProg + "/" + cle.getKey() + " I");
			}
			cible.add("return");
			cible.add(".end method\n");
		}
		if (p.listeFonc() != null)
			p.listeFonc().accepter(this); //pour les fonctions
		listeVarsCourants = p.varsLocals();
		cible.debutProgramme(p.varsLocals().size()); //programme principal
		if (p.varsLocals().size() != 0) {
			cible.add("; Initialisation variables");
			for (Map.Entry<Entree, Symbole> cle : p.varsLocals().entrySet()) {
				Entree e = cle.getKey();
				Symbole s = cle.getValue();
				if (s.type() instanceof TypeTableau) {
					TypeTableau t = (TypeTableau) s.type();
					// le 0 en paramètre indique l'indice de la dimension du tableau
					cible.add("ldc " + (t.borneSuperieur(0) - t.borneInferieure(0) + 1)); // taille tableau
					cible.add("newarray int"); //construction tableau
					cible.add("astore " + s.indiceVar()); //enregistrement tableau dans la variable tableau
				} else {
		            cible.add("ldc 0");
		            cible.add("istore " + s.indiceVar());
					}
			}
		    cible.add("; fin initialisation");
		}
		if (p.corps() != null)
			p.corps().accepter(this);//corps du programme principal
		cible.finFichier();
		return null;
	}

    public Object visiter(ListeFonctions lf) {
		lf.accepter(this);
		return null;
	}

	public Object visiter(Fonction f) {
		cible.addDebutFonction(f.nom(), f.varsLocals().size(), f.typesParams(), f.typeRetour());
		if (f.varsLocals().size() != 0) {
			cible.add("; Initialisation variables");
			for (Map.Entry<Entree, Symbole> cle : f.varsLocals().entrySet()) {
				Entree e = cle.getKey();
				Symbole s = cle.getValue();
				if (s.type() instanceof TypeTableau) {
					TypeTableau t = (TypeTableau) s.type();
					if (!f.paramsFonc().containsKey(e)) {
						// le 0 en paramètre indique l'indice de la dimension du tableau
						cible.add("ldc " + (t.borneSuperieur(0) - t.borneInferieure(0) + 1)); // taille tableau
						cible.add("newarray int"); //construction tableau
						cible.add("astore " + s.indiceVar()); //enregistrement tableau dans la variable tableau
					}
				} else {
					if (!f.paramsFonc().containsKey(e)) {
			            cible.add("ldc 0");
			            cible.add("istore " + s.indiceVar());
					}
				}
			}
		    cible.add("; fin initialisation");
		}
		listeVarsCourants = f.varsLocals();
		if (f.corps() != null)
			f.corps().accepter(this);
		if (f.typeRetour().equals("[I")) {
			cible.add("ldc 0"); // taille tableau
			cible.add("newarray int"); //construction tableau
			cible.add("areturn");
		} else {
			cible.add("ldc 0");
			cible.add("ireturn");
		}
		cible.addFinFonction();
		return null;
	}

    public Object visiter (RetourneFonction r) {
		r.expr().accepter(this);
		if (r.type().typeRetour() instanceof TypeTableau) {
			cible.add("areturn");
		} else {
			cible.add("ireturn");
		}
		return null;
	}

	public Object visiter(AppelQualifie a) {
		if (a.listeParam() != null) {
			a.listeParam().accepter(this);
		}
		String s = "";
		for (String str : a.typesParams()) {
			s += str;
		}
		cible.add("invokestatic " + nomProg + "." + a.ident().nom() + "(" + s + ")" + a.typeRetour());
		return null;
	}

	public Object visiter(Arithmetique a) {
		a.gauche().accepter(this);
		a.droite().accepter(this);
		if (a.operateur().equals("+")) {
			cible.add("iadd");
			return null;
		}
		if (a.operateur().equals("-")) {
			cible.add("isub");
			return null;
		}
		if (a.operateur().equals("*")) {
			cible.add("imul");
			return null;
		}
		if (a.operateur().equals("/")) {
			cible.add("idiv");
			return null;
		}
		if (a.operateur().equals("%")) {
			cible.add("irem");
			return null;
		}
		return null;
	}

    public Object visiter(Relation a) {
		a.gauche().accepter(this);
		a.droite().accepter(this);
		if (a.operateur().equals("<="))
			cible.add("if_icmple label" + a.hashCode() + "_0");
		if (a.operateur().equals("<"))
			cible.add("if_icmplt label" + a.hashCode() + "_0");
		if (a.operateur().equals(">="))
			cible.add("if_icmpge label" + a.hashCode() + "_0");
		if (a.operateur().equals(">"))
			cible.add("if_icmpgt label" + a.hashCode() + "_0");
		if (a.operateur().equals("=="))
			cible.add("if_icmpeq label" + a.hashCode() + "_0");
		if (a.operateur().equals("<>"))
			cible.add("if_icmpne label" + a.hashCode() + "_0");
		cible.add("goto label" + a.hashCode() + "_1");
		cible.add("label" + a.hashCode() + "_0:");
		cible.add("ldc 1");
		cible.add("goto endif" + a.hashCode() + "_0");
		cible.add("label" + a.hashCode() + "_1:");
		cible.add("ldc 0");
		cible.add("endif" + a.hashCode() + "_0:");
		return null;
	}

	public Object visiter(Affectation a) {
		if (a.destination() instanceof CaseTableau) {
			CaseTableau cas = (CaseTableau) a.destination();
			Entree e = new Entree(new Ident(cas.ident().nom()));
			Symbole s = listeVarsCourants.get(e);
			if (s != null) {
				int indiceVar = s.indiceVar();
				TypeTableau t = (TypeTableau) s.type();
				cible.add("aload " + indiceVar);
				cas.indice().accepter(this);
				cible.add("ldc " + t.borneInferieure(0));
				cible.add("isub");
				a.source().accepter(this);
				cible.add("iastore");
			}
		} else {
			a.source().accepter(this);
			Idf i = (Idf) a.destination();
			Entree e = new EntreeEntBool(new Ident(i.nom()));
			Symbole s = listeVarsCourants.get(e);
			if (s != null) {
				int indiceVar = s.indiceVar();
				cible.add("istore " + indiceVar);
			}
		}
        return null;
    }

	public Object visiter(Condition c) {
		c.condition().accepter(this);
		cible.add("ifne label" + c.hashCode() + "_0"); // succeeds if and only if value ≠ 0
		cible.add("goto label" + c.hashCode() + "_1");
		cible.add("label" + c.hashCode() + "_0:");
		c.alors().accepter(this);
		cible.add("goto endif" + c.hashCode() + "_0");
		cible.add("label" + c.hashCode() + "_1:");
		if (c.sinon() != null) {
			c.sinon().accepter(this);
		}
		cible.add("endif" + c.hashCode() + "_0:");
		return null;
	}

    public Object visiter(TantQue t) {
		cible.add("label" + t.hashCode() + ":");
		t.condition().accepter(this);
		cible.add("ifeq endwhile" + t.hashCode());
		t.corps().accepter(this);
		cible.add("goto label" + t.hashCode());
		cible.add("endwhile" + t.hashCode() + ":");
		return null;
	}

    public Object visiter(Pour p) {
		p.borneInf().accepter(this);
		Entree e = new Entree(new Ident(p.compteur().nom()));
		Symbole s = listeVarsCourants.get(e);
		if (s != null) {
			int indiceVar = s.indiceVar();
			cible.add("istore " + indiceVar);
			cible.add("label" + p.hashCode() + ":");
			cible.add("iload " + indiceVar);
			p.borneSup().accepter(this);
			cible.add("if_icmpgt endFor" + p.hashCode());
			p.corps().accepter(this);
			cible.add("iinc " + indiceVar + " 1"); // incrémenter le compteur de 1
			cible.add("goto label" + p.hashCode());
			cible.add("endFor" + p.hashCode() + ":");
		}
		return null;
	}

    public Object visiter (Nombre n) {
		cible.add("ldc " + n.valeur());
		return null;
    }

    public Object visiter (Booleen n) {
		cible.add("ldc " + n.valeur());
		return null;
	}

    public Object visiter (ChaineCar s) {
		cible.add("ldc " + s.valeur());
		return null;
	}

    public Object visiter (Idf i) {
		Entree e = new Entree(new Ident(i.nom()));
		Symbole s = listeVarsCourants.get(e);
		if (s != null) {
			int indiceVar = s.indiceVar();
			if (s.type() instanceof TypeTableau) {
				cible.add("aload " + indiceVar);
			} else {
				cible.add("iload " + indiceVar);
			}
			return indiceVar;
		} else {
			s = listeConstantes.get(e);
			if (s != null) {
				if (s instanceof SymboleConstEnt) {
					cible.add("getstatic " + nomProg + "/" + i.nom() + " I");
				}
			}
		}
		return null;
    }

    public Object visiter (CaseTableau c) {
		c.ident().accepter(this); //chargement du tableau dans la pile
		c.indice().accepter(this); //chargement de l'indice dans la pile
		Entree e = new Entree(new Ident(c.ident().nom()));
		Symbole s = listeVarsCourants.get(e);
		if (s != null) {
			TypeTableau t = (TypeTableau) s.type();
			cible.add("ldc " + t.borneInferieure(0));
			cible.add("isub");
			cible.add("iaload");
		}
		return null;
	}

	public Object visiter(Bloc b) {
		b.accepter(this);
		return null;
	}

    public Object visiter (Instruction i) {
		i.accepter(this);
		return null;
    }

    public Object visiter (Expression i) {
		i.accepter(this);
		return null;
    }

    public Object visiter (Ecrire b) {
		b.expr().accepter(this);
		Type t = b.expr().type();
		if (b.expr() instanceof ChaineCar) {
			if (!ecrireChaineInserree) {
				cible.addAll(cible.enteteNum(), cible.addEcrireChaineMethod());
				ecrireChaineInserree = true;
			}
			cible.add("invokestatic " + nomProg + ".print(Ljava/lang/String;)V ; appel à la méthode affichage chaine caractères");
		}
		else {
			if (!ecrireEntInserree) {
				cible.addAll(cible.enteteNum(), cible.addEcrireMethod());
				ecrireEntInserree = true;
			}
			cible.add("invokestatic " + nomProg + ".print(I)V ; appel à la méthode affichage entier");
		}
		return null;
	}

	public Object visiter(Lire l) {
		Entree e = new EntreeEntBool(new Ident(l.ident().nom()));
		Symbole s = listeVarsCourants.get(e);
		if (s != null) {
			int indiceVar = s.indiceVar();
			if (!lireEntInserree) {
				cible.addAll(cible.enteteNum(), cible.addLireMethod());
				lireEntInserree = true;
			}
			cible.add("invokestatic " + nomProg + ".read()I ; appel à la méthode lire entier");
			cible.add("istore " + indiceVar);
		}
		return null;
	}
}
