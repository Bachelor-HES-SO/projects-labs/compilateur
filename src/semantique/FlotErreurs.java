package semantique;

import java.util.Stack;

public class FlotErreurs extends Stack<String> {
    private static FlotErreurs flotErreurs = new FlotErreurs();

    public static FlotErreurs getInstance() {
        return flotErreurs;
    }

    public String toString() {
        if (this.isEmpty()) {
            return "Pas d'erreur de compilation!";
        }
        String res = "";
        while (!this.isEmpty()) {
            res = this.pop() + "\n" + res;
        }
        return res;
    }
}
